// --- Data management ---

class BasicAuth {
    constructor(hostname, username, password) {
        this.hostname = hostname;
        this.headers = {
            authorization: 'Basic ' + btoa(username + ':' + password),
        };
    }

    static fromLocalStorage() {
        const hostname = localStorage.getItem('hostname');
        const username = localStorage.getItem('username');
        const password = localStorage.getItem('password');
        return hostname && username && password ? new BasicAuth(
            hostname,
            username,
            password,
        ) : null;
    }
}

class JMAP {
    constructor(auth) {
        this.auth = auth || BasicAuth.fromLocalStorage();
        this.accounts = {};
        const session = localStorage.getItem('session');
        if (session) {
            this.session = JSON.parse(session);
        } else {
            this._session = null;
        }
    }

    get session() {
        return this._session;
    }

    set session(session) {
        this._session = session;
        this.accounts = Object.entries(session.accounts)
            .reduce((accounts, [accountId, account]) => {
                account = accounts[accountId] = Object.create(account);
                account.id = accountId;
                return accounts;
            }, {});
    }

    async loadSession(noRedirect) {
        if (!this.session) {
            await this.fetchSession(noRedirect);
        }
    }

    async fetchSession(noRedirect) {
        if (!this.auth) {
            top.location.href = 'login.html';
            return;
        }
        let response;
        try {
            response = await fetch(`https://${this.auth.hostname}/.well-known/jmap`, {
                headers: {
                    'accept': 'application/json',
                    ...this.auth.headers,
                },
            });
        } catch (e) {
            if (noRedirect) {
                e.reason = 'session-fetch-network-error';
            } else {
                top.location.href = urlEncode`login.html?error=session-fetch-network-error&exception=${e}`;
            }
            throw e;
        }
        const session = this.session = await response.json();
        localStorage.setItem('session', JSON.stringify(session));
    }

    async call(request) {
        const response = await fetch(this.session.apiUrl, {
            method: 'POST',
            headers: {
                'content-type': 'application/json',
                'accept': 'application/json',
                ...this.auth.headers,
            },
            body: typeof request === 'string' ? request : JSON.stringify(request),
        });
        return await response.json();
    }

    async simpleCall(methodCalls) {
        return (await this.call({
            using: Object.keys(this.session.capabilities),
            methodCalls: methodCalls,
        })).methodResponses;
    }
}

function createIndex(list, field, index) {
    return list.reduce((index, item) => {
        if (item[field]) {
            index[item[field]] = item;
        }
        return index;
    }, index || {});
}

function parseAddressList(addressList) {
    return addressList.split(',').map(part => {
        part = part.trim();
        // FIXME: this ain’t exactly spec-compliant.
        let parsed = /^(?:(?:"(.+)"|(.+)) )<(.+)>$|^(.+)$/.exec(part);
        return {
            name: parsed[1] || parsed[2] || null,
            email: parsed[3] || parsed[4],
        };
    });
}

function formatAddresses(addresses) {
    return !addresses ? '???' : addresses.map(address => {
        if (!address) {
            return '???';
        } else if (address.name) {
            // FIXME: insufficient
            return `${address.name} <${address.email}>`;
        } else {
            return address.email;
        }
    }).join(', ');
}

// --- Rendering ---

function el(tagName, attrs, children) {
    if (Array.isArray(attrs)) {
        children = attrs;
        attrs = {};
    }
    const element = document.createElement(tagName);
    for (const [name, value] of Object.entries(attrs || {})) {
        if (value != null) {
            element.setAttribute(name, value);
        }
    }
    if (children) {
        element.append(...children);
    }
    return element;
}

function draw(children) {
    document.body.innerHTML = '';
    document.body.append(...children);
}

async function J(fn) {
    const jmap = new JMAP();
    await jmap.loadSession();
    return fn(jmap);
}

function urlEncode(strings, ...partsToEscape) {
    let out = strings[0];
    let len = strings.length;
    for (let i = 1; i < len; i++) {
        out += encodeURIComponent(partsToEscape[i - 1]);
        out += strings[i];
    }
    return out;
}
