# Fair Dinkum Email Client™

This is a very simple JMAP email client, intended for demonstrations, pedagogy and as an artefact to roughly reproduce while live coding in conference talks.

Despite its simplicity and rudimentary nature, it is nonetheless fair dinkum: if you were mad enough, you could probably use it on a day-to-day basis.

Features currently implemented:

1. List mailboxes from multiple accounts
2. View the contents of mailboxes
3. Read text/plain emails
4. Compose new emails, and reply to old emails

Features not yet implemented:

1. More details on emails (e.g. which mailboxes they’re in)
2. Image support
3. Other attachments support
4. Search
5. Event source handling (very simple, just reloading the mailbox list when suitable)

Features that may never be implemented (though they may, they’re just *super* low priority, and if they get in the way of pedagogical simplicity they won’t happen):

1. text/html support (it’s very easy to do insecurely, and involved to do securely)
2. Forwarding

## Running it

Open index.html. All modern browsers should work.

It probably needs to be run in a secure context, such as HTTPS or HTTP on a localhost hostname.

It stores your username and password in local storage, so there’s a certain security concern there.
